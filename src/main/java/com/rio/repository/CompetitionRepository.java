package com.rio.repository;

import com.rio.entity.Competition;
import org.springframework.data.jpa.repository.JpaRepository;
public interface CompetitionRepository extends JpaRepository<Competition, Integer> {
}
