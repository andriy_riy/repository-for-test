package com.rio.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "coordinate")
public class Coordinate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "coordinate_id", nullable = false, updatable = false, unique = true, length = 11)
    private Integer id;

    @Column(name = "latitude_degrees", nullable = false, length = 2)
    private Byte latitudeDegrees;

    @Column(name = "latitude_minute", nullable = false, length = 2)
    private Byte latitudeMinute;

    @Column(name = "latitude_second", nullable = false, length = 2)
    private Byte latitudeSecond;

    @Enumerated(EnumType.STRING)
    @Column(name = "latitude", nullable = false)
    private Latitude latitude;

    @Column(name = "longitude_degrees", nullable = false, length = 3)
    private Byte longitudeDegrees;

    @Column(name = "longitude_minute", nullable = false, length = 2)
    private Byte longitudeMinute;

    @Column(name = "longitude_second", nullable = false, length = 2)
    private Byte longitudeSecond;

    @Enumerated(EnumType.STRING)
    @Column(name = "longitude", nullable = false)
    private Longitude longitude;

    public enum Latitude{
        North, South
    }

    public enum Longitude{
        West, East
    }

    public Coordinate() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getLatitudeDegrees() {
        return latitudeDegrees;
    }

    public void setLatitudeDegrees(Byte latitudeDegrees) {
        this.latitudeDegrees = latitudeDegrees;
    }

    public Byte getLatitudeMinute() {
        return latitudeMinute;
    }

    public void setLatitudeMinute(Byte latitudeMinute) {
        this.latitudeMinute = latitudeMinute;
    }

    public Byte getLatitudeSecond() {
        return latitudeSecond;
    }

    public void setLatitudeSecond(Byte latitudeSecond) {
        this.latitudeSecond = latitudeSecond;
    }

    public Latitude getLatitude() {
        return latitude;
    }

    public void setLatitude(Latitude latitude) {
        this.latitude = latitude;
    }

    public Byte getLongitudeDegrees() {
        return longitudeDegrees;
    }

    public void setLongitudeDegrees(Byte longitudeDegrees) {
        this.longitudeDegrees = longitudeDegrees;
    }

    public Byte getLongitudeMinute() {
        return longitudeMinute;
    }

    public void setLongitudeMinute(Byte longitudeMinute) {
        this.longitudeMinute = longitudeMinute;
    }

    public Byte getLongitudeSecond() {
        return longitudeSecond;
    }

    public void setLongitudeSecond(Byte longitudeSecond) {
        this.longitudeSecond = longitudeSecond;
    }

    public Longitude getLongitude() {
        return longitude;
    }

    public void setLongitude(Longitude longitude) {
        this.longitude = longitude;
    }
}
