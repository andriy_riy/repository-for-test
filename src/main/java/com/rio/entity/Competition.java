package com.rio.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "competition")
public class Competition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "competition_id", nullable = false, updatable = false, unique = true, length = 11)
    private Integer id;

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "pigeon_count", nullable = false)
    private Integer pigeonCount;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "coordinate_id")
    private Coordinate coordinate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "competition_user",
               joinColumns = @JoinColumn(name = "competition_id"),
               inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> users;

    @OneToMany(mappedBy = "competition", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<CompetitionResult> competitionResults;

    public enum Status{
        NotStarted, Started, Completed
    }

    public Competition() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Integer getPigeonCount() {
        return pigeonCount;
    }

    public void setPigeonCount(Integer pigeonCount) {
        this.pigeonCount = pigeonCount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<CompetitionResult> getCompetitionResults() {
        return competitionResults;
    }

    public void setCompetitionResults(Set<CompetitionResult> competitionResults) {
        this.competitionResults = competitionResults;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Competition that = (Competition) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(pigeonCount, that.pigeonCount) &&
                status == that.status &&
                Objects.equals(coordinate, that.coordinate) &&
                Objects.equals(users, that.users) &&
                Objects.equals(competitionResults, that.competitionResults);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, startDate, pigeonCount, status, coordinate, users, competitionResults);
    }
}
