package com.rio.repository;

import com.rio.entity.Pigeon;
import com.rio.entity.PigeonImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PigeonImageRepository extends JpaRepository<PigeonImage, Integer> {
    PigeonImage findByPigeon(Pigeon pigeon);
}
