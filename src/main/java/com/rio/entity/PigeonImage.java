package com.rio.entity;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Date;

@Entity
@Table(name = "pigeon_image")
public class PigeonImage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pigeon_image_id", nullable = false, updatable = false, unique = true, length = 11)
    private Integer id;

    @Column(name = "content", columnDefinition="blob", nullable = false)
    private Blob image;

    @Column(name = "file_name", length = 255)
    private String fileName;

    @Column(name = "date_add")
    private Date dateAdd;

    @ManyToOne()
    @JoinColumn(name = "pigeon_id")
    private Pigeon pigeon;

    public PigeonImage() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(Date dateAdd) {
        this.dateAdd = dateAdd;
    }

    public Pigeon getPigeon() {
        return pigeon;
    }

    public void setPigeon(Pigeon pigeon) {
        this.pigeon = pigeon;
    }
}