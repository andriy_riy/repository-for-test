package com.rio.controller;

import com.rio.entity.Pigeon;
import com.rio.entity.PigeonImage;
import com.rio.service.PigeonImageService;
import com.rio.service.PigeonService;
import com.rio.utill.CustomErrorType;
import org.hibernate.engine.jdbc.BlobProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/pigeon")
public class PigeonController {

    @Autowired
    private PigeonService pigeonService;

    @Autowired
    private PigeonImageService pigeonImageService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<Pigeon> getPigeon(@PathVariable("id") int id) {
        Pigeon pigeon = pigeonService.getPigeonById(id);
        if (pigeon == null) {
            return new ResponseEntity(new CustomErrorType("Pigeon with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(pigeon, HttpStatus.OK);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<List<Pigeon>> getAllLPigeons() {
        List<Pigeon> pigeons = pigeonService.getAll();
        if (pigeons.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(pigeons, HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addPigeon(@RequestBody Pigeon pigeon, UriComponentsBuilder ucBuilder) {
        pigeonService.addPigeon(pigeon);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(pigeon.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Pigeon> updatePigeon(@PathVariable("id") int id, @RequestBody Pigeon pigeon) {

        Pigeon currentPigeon = pigeonService.getPigeonById(id);

        if (currentPigeon == null) {
            return new ResponseEntity(new CustomErrorType("Unable to update. Pigeon with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        currentPigeon.setSex(pigeon.getSex());
        currentPigeon.setCountry(pigeon.getCountry());
        currentPigeon.setYear(pigeon.getYear());
        currentPigeon.setRingNumber(pigeon.getRingNumber());
        //currentPigeon.setImage(pigeon.getImage());
        currentPigeon.setOwner(pigeon.getOwner());
        currentPigeon.setCompetitionResults(pigeon.getCompetitionResults());

        pigeonService.editPigeon(currentPigeon);
        return new ResponseEntity<>(currentPigeon, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Pigeon> deletePigeon(@PathVariable("id") int id) {

        Pigeon pigeon = pigeonService.getPigeonById(id);
        if (pigeon == null) {
            return new ResponseEntity(new CustomErrorType("Unable to delete. Pigeon with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        pigeonService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "get/{id}/image", method = RequestMethod.GET,  produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getPigeonImage(@PathVariable("id") int id) throws SQLException, IOException {
        Pigeon pigeon = pigeonService.getPigeonById(id);

        if (pigeon == null) {
            return new ResponseEntity(new CustomErrorType("Pigeon with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }

        PigeonImage image = pigeonImageService.getImageByPigeon(pigeon);

        if (image == null) {
            return new ResponseEntity(new CustomErrorType("Pigeon image with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }

        InputStream inputStream = image.getImage().getBinaryStream();

        byte[] content = new byte[inputStream.available()];
        inputStream.read(content);

        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(content);
    }

    @RequestMapping(value = "save-image/{id}", method = RequestMethod.POST)
    public String uploadImage(@RequestParam("file") MultipartFile image, @PathVariable("id") int id) throws IOException {

        PigeonImage pigeonImage = new PigeonImage();
        pigeonImage.setDateAdd(new Date());
        Pigeon pigeon = pigeonService.getPigeonById(id);
        pigeonImage.setPigeon(pigeon);
        pigeonImage.setFileName(image.getOriginalFilename());
        pigeonImage.setImage(BlobProxy.generateProxy(image.getBytes()));

        pigeonImageService.addPigeonImage(pigeonImage);
        return "Image successfully upload.";
    }
}
