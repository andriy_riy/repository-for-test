package com.rio.entity;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "pigeon")
public class Pigeon {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pigeon_id", nullable = false, updatable = false, unique = true, length = 11)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex", nullable = false)
    private Sex sex;

    @Column(name = "country", nullable = false, length = 10)
    private String country;

    @Column(name = "year", nullable = false, length = 4)
    private Short year;

    @Column(name = "ring_number", nullable = false, length = 15)
    private String ringNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User owner;

    @OneToMany(mappedBy = "pigeon", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<CompetitionResult> competitionResults;

    public enum Sex{
        Male, Female
    }

    public Pigeon() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Short getYear() {
        return year;
    }

    public void setYear(Short year) {
        this.year = year;
    }

    public String getRingNumber() {
        return ringNumber;
    }

    public void setRingNumber(String ringNumber) {
        this.ringNumber = ringNumber;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Set<CompetitionResult> getCompetitionResults() {
        return competitionResults;
    }

    public void setCompetitionResults(Set<CompetitionResult> competitionResults) {
        this.competitionResults = competitionResults;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pigeon pigeon = (Pigeon) o;
        return Objects.equals(id, pigeon.id) &&
                sex == pigeon.sex &&
                Objects.equals(country, pigeon.country) &&
                Objects.equals(year, pigeon.year) &&
                Objects.equals(ringNumber, pigeon.ringNumber) &&
                Objects.equals(owner, pigeon.owner) &&
                Objects.equals(competitionResults, pigeon.competitionResults);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, sex, country, year, ringNumber, owner, competitionResults);
    }
}
