package com.rio.service.impl;

import com.rio.entity.User;
import com.rio.entity.UserImage;
import com.rio.repository.UserImageRepository;
import com.rio.service.UserImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserImageServiceImpl implements UserImageService {

    @Autowired
    private UserImageRepository userImageRepository;

    @Override
    public UserImage addUserImage(UserImage userImage) { return userImageRepository.saveAndFlush(userImage);
    }

    @Override
    public void delete(Integer id) {
        userImageRepository.deleteById(id);
    }

    @Override
    public UserImage editUserImage(UserImage userImage) {
        return userImageRepository.saveAndFlush(userImage);
    }

    @Override
    public List<UserImage> getAll() {
        return userImageRepository.findAll();
    }

    @Override
    public UserImage getImageByUser(User user){
        return userImageRepository.findByUser(user);
    }

    @Override
    public UserImage getImageById(Integer id) {
        return userImageRepository.findById(id).get();
    }
}