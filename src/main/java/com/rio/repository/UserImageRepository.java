package com.rio.repository;

import com.rio.entity.User;
import com.rio.entity.UserImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserImageRepository extends JpaRepository<UserImage, Integer> {
    UserImage findByUser(User user);
}
