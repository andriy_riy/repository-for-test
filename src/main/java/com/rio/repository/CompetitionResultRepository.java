package com.rio.repository;

import com.rio.entity.CompetitionResult;
import org.springframework.data.repository.CrudRepository;

public interface CompetitionResultRepository extends CrudRepository<CompetitionResult, Integer> {
}
