package com.rio.service.impl;

import com.rio.entity.Pigeon;
import com.rio.entity.PigeonImage;
import com.rio.repository.PigeonImageRepository;
import com.rio.service.PigeonImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PigeonImageServiceImpl implements PigeonImageService {
    @Autowired
    private PigeonImageRepository pigeonImageRepository;

    @Override
    public PigeonImage addPigeonImage(PigeonImage pigeonImage) {
        return pigeonImageRepository.saveAndFlush(pigeonImage);
    }

    @Override
    public void delete(Integer id) {
        pigeonImageRepository.deleteById(id);
    }

    @Override
    public PigeonImage editPigeonImage(PigeonImage pigeonImage) {
        return pigeonImageRepository.saveAndFlush(pigeonImage);
    }

    @Override
    public List<PigeonImage> getAll() {
        return pigeonImageRepository.findAll();
    }

    @Override
    public PigeonImage getImageByPigeon(Pigeon pigeon) {
        return pigeonImageRepository.findByPigeon(pigeon);
    }

    @Override
    public PigeonImage getImageById(Integer id) {
        return pigeonImageRepository.findById(id).get();
    }
}