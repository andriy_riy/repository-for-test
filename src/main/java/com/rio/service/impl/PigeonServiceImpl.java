package com.rio.service.impl;

import com.rio.entity.Pigeon;
import com.rio.repository.PigeonRepository;
import com.rio.service.PigeonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PigeonServiceImpl implements PigeonService {
    @Autowired
    private PigeonRepository pigeonRepository;

    @Override
    public Pigeon addPigeon(Pigeon pigeon) {
        return pigeonRepository.saveAndFlush(pigeon);
    }

    @Override
    public void delete(Integer id) {
        pigeonRepository.deleteById(id);
    }

    @Override
    public Pigeon getByRingNumber(String ringNumber) {
        return pigeonRepository.findByRingNumber(ringNumber);
    }

    @Override
    public Pigeon editPigeon(Pigeon pigeon) {
        return pigeonRepository.saveAndFlush(pigeon);
    }

    @Override
    public List<Pigeon> getAll() {
        return pigeonRepository.findAll();
    }

    @Override
    public Pigeon getPigeonById(Integer id){
        return pigeonRepository.findById(id).get();
    }
}
