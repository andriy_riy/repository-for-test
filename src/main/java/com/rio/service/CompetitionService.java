package com.rio.service;

import com.rio.entity.Competition;

import java.util.List;

public interface CompetitionService {
    Competition addCompetition(Competition competition);
    void delete(Integer id);
    Competition editCompetition(Competition competition);
    Competition getCompetitionById(Integer id);
    List<Competition> getAll();
}
