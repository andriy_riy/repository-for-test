package com.rio.entity;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name = "competition_result")
public class CompetitionResult {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "competition_result_id", nullable = false, updatable = false, unique = true, length = 11)
    private Integer id;

    @Column(name = "time")
    private Time time;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "competition_id")
    private Competition competition;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pigeon_id")
    private Pigeon pigeon;

    public CompetitionResult() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public Pigeon getPigeon() {
        return pigeon;
    }

    public void setPigeon(Pigeon pigeon) {
        this.pigeon = pigeon;
    }
}
