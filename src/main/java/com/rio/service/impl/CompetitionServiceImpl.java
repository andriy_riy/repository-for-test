package com.rio.service.impl;

import com.rio.entity.Competition;
import com.rio.repository.CompetitionRepository;
import com.rio.service.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompetitionServiceImpl implements CompetitionService{
    @Autowired
    private CompetitionRepository competitionRepository;


    @Override
    public Competition addCompetition(Competition competition) {
        return competitionRepository.saveAndFlush(competition);
    }

    @Override
    public void delete(Integer id) {
        competitionRepository.deleteById(id);
    }

    @Override
    public Competition editCompetition(Competition competition) {
        return competitionRepository.saveAndFlush(competition);
    }

    @Override
    public List<Competition> getAll() {
        return competitionRepository.findAll();
    }

    @Override
    public Competition getCompetitionById(Integer id){
        return competitionRepository.findById(id).get();
    }
}
