package com.rio.service;

import com.rio.entity.Pigeon;
import com.rio.entity.PigeonImage;

import java.util.List;

public interface PigeonImageService {
    PigeonImage addPigeonImage(PigeonImage pigeonImage);
    void delete(Integer id);
    PigeonImage editPigeonImage(PigeonImage pigeonImage);
    List<PigeonImage> getAll();
    PigeonImage getImageByPigeon(Pigeon pigeon);
    PigeonImage getImageById(Integer id);
}
