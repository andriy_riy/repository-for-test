package com.rio.controller;

import com.rio.entity.Competition;
import com.rio.service.CompetitionService;
import com.rio.utill.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;


@RestController
@RequestMapping("/competition")
public class CompetitionController {

    @Autowired
    private CompetitionService competitionService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<Competition> getCompetition(@PathVariable("id") int id) {
        Competition competition = competitionService.getCompetitionById(id);
        if (competition == null) {
            return new ResponseEntity(new CustomErrorType("Competition with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(competition, HttpStatus.OK);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<List<Competition>> getAllLCompetition() {
        List<Competition> competitions = competitionService.getAll();
        if (competitions.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(competitions, HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addCompetition(@RequestBody Competition competition, UriComponentsBuilder ucBuilder) {
        competitionService.addCompetition(competition);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/competition/{id}").buildAndExpand(competition.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Competition> updateCompetition(@PathVariable("id") int id, @RequestBody Competition competition) {

        Competition currentCompetition = competitionService.getCompetitionById(id);

        if (currentCompetition == null) {
            return new ResponseEntity(new CustomErrorType("Unable to update. Competition with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        currentCompetition.setStartDate(competition.getStartDate());
        currentCompetition.setPigeonCount(competition.getPigeonCount());
        currentCompetition.setPigeonCount(competition.getPigeonCount());
        currentCompetition.setStatus(competition.getStatus());
        currentCompetition.setCoordinate(competition.getCoordinate());
        currentCompetition.setUsers(competition.getUsers());
        currentCompetition.setCompetitionResults(competition.getCompetitionResults());

        competitionService.editCompetition(currentCompetition);
        return new ResponseEntity<>(currentCompetition, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Competition> deleteCompetition(@PathVariable("id") int id) {

        Competition competition = competitionService.getCompetitionById(id);
        if (competition == null) {
            return new ResponseEntity(new CustomErrorType("Unable to delete. Competition with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        competitionService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
