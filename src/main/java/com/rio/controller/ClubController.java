package com.rio.controller;

import com.rio.entity.Club;
import com.rio.service.ClubService;
import com.rio.utill.CustomErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/club")
public class ClubController {

    @Autowired
    private ClubService clubService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<Club> getClub(@PathVariable("id") int id) {
        Club club = clubService.getClubById(id);
        if (club == null) {
            return new ResponseEntity(new CustomErrorType("Club with id " + id
                   + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Club>(club, HttpStatus.OK);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<List<Club>> getAllLClubs() {
        List<Club> clubs = clubService.getAll();
        if (clubs.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(clubs, HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addClub(@RequestBody Club club, UriComponentsBuilder ucBuilder) {
        clubService.addClub(club);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/club/{id}").buildAndExpand(club.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Club> updateClub(@PathVariable("id") int id, @RequestBody Club club) {

        Club currentClub = clubService.getClubById(id);

        if (currentClub == null) {
            return new ResponseEntity(new CustomErrorType("Unable to update. Club with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        currentClub.setName(club.getName());
        currentClub.setUserCount(club.getUserCount());

        clubService.editClub(currentClub);
        return new ResponseEntity<>(currentClub, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Club> deleteClub(@PathVariable("id") int id) {

        Club club = clubService.getClubById(id);
        if (club == null) {
            return new ResponseEntity(new CustomErrorType("Unable to delete. Club with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        clubService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}