package com.rio.service;

import com.rio.entity.Pigeon;

import java.util.List;

public interface PigeonService {
    Pigeon addPigeon(Pigeon pigeon);
    void delete(Integer id);
    Pigeon getByRingNumber(String ringNumber);
    Pigeon getPigeonById(Integer id);
    Pigeon editPigeon(Pigeon pigeon);
    List<Pigeon> getAll();
}
