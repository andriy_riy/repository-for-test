package com.rio.service;

import com.rio.entity.User;
import com.rio.entity.UserImage;

import java.util.List;

public interface UserImageService {
    UserImage addUserImage(UserImage userImage);
    void delete(Integer id);
    UserImage editUserImage(UserImage userImage);
    List<UserImage> getAll();
    UserImage getImageByUser(User user);
    UserImage getImageById(Integer id);
}
