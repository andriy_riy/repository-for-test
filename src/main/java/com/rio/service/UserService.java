package com.rio.service;

import com.rio.entity.User;

import java.util.List;

public interface UserService {
    User addUser(User user);
    void delete(Integer id);
    User editUser(User user);
    User getUserById(Integer id);
    List<User> getAll();
}
