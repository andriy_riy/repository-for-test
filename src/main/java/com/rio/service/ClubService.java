package com.rio.service;

import com.rio.entity.Club;

import java.util.List;
import java.util.Optional;

public interface ClubService {
    Club addClub(Club club);
    void delete(Integer id);
    Club getClubByName(String name);
    Club getClubById(Integer id);
    Club editClub(Club club);
    List<Club> getAll();
}
