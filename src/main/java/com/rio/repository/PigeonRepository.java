package com.rio.repository;

import com.rio.entity.Pigeon;
import com.rio.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PigeonRepository extends JpaRepository<Pigeon, Integer> {
    Pigeon findByRingNumber(String ringNumber);
    List<Pigeon> findAllByOwner(User owner);
}
