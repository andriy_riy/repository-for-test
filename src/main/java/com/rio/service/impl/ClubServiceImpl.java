package com.rio.service.impl;

import com.rio.entity.Club;
import com.rio.repository.ClubRepository;
import com.rio.service.ClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClubServiceImpl implements ClubService{

    @Autowired
    private ClubRepository clubRepository;

    @Override
    public Club addClub(Club club) {
        return clubRepository.saveAndFlush(club);
    }

    @Override
    public void delete(Integer id) {
        clubRepository.deleteById(id);
    }

    @Override
    public Club getClubByName(String name) {
        return clubRepository.findByName(name);
    }

    @Override
    public Club editClub(Club club) {
        return clubRepository.saveAndFlush(club);
    }

    @Override
    public List<Club> getAll() {
        return clubRepository.findAll();
    }

    @Override
    public Club getClubById(Integer id){
        return clubRepository.findById(id).get();
    }
}
