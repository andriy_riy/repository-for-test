package com.rio.controller;

import com.rio.entity.User;
import com.rio.entity.UserImage;
import com.rio.service.UserImageService;
import com.rio.service.UserService;
import com.rio.utill.CustomErrorType;
import org.hibernate.engine.jdbc.BlobProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserImageService userImageService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUser(@PathVariable("id") int id) {
        User user = userService.getUserById(id);
        if (user == null) {
            return new ResponseEntity(new CustomErrorType("User with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllLUsers() {
        List<User> users = userService.getAll();
        if (users.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addUser(@RequestBody User user, UriComponentsBuilder ucBuilder) {
        userService.addUser(user);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("id") int id, @RequestBody User user) {

        User currentUser = userService.getUserById(id);

        if (currentUser == null) {
            return new ResponseEntity(new CustomErrorType("Unable to update. User with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        currentUser.setFirstName(user.getFirstName());
        currentUser.setLastName(user.getLastName());
        currentUser.setEmail(user.getEmail());
        currentUser.setPassword(user.getPassword());
        currentUser.setPhoneNumber(user.getPhoneNumber());
        currentUser.setRole(user.getRole());
        currentUser.setClub(user.getClub());
        //currentUser.setPigeons(user.getPigeons());
        currentUser.setCompetitions(user.getCompetitions());

        userService.editUser(currentUser);
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") int id) {

        User user = userService.getUserById(id);
        if (user == null) {
            return new ResponseEntity(new CustomErrorType("Unable to delete. User with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "get/{id}/image", method = RequestMethod.GET,  produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getUserImage(@PathVariable("id") int id) throws SQLException, IOException {
        User user = userService.getUserById(id);

        if (user == null) {
            return new ResponseEntity(new CustomErrorType("User with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }

        UserImage image = userImageService.getImageByUser(user);

        if (image == null) {
            return new ResponseEntity(new CustomErrorType("User image with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }

        InputStream inputStream = image.getImage().getBinaryStream();

        byte[] content = new byte[inputStream.available()];
        inputStream.read(content);

        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(content);
    }

    @RequestMapping(value = "save-image/{id}", method = RequestMethod.POST)
    public String uploadImage(@RequestParam("file") MultipartFile image, @PathVariable("id") int id) throws IOException {

        UserImage userImage = new UserImage();
        userImage.setDateAdd(new Date());
        User user = userService.getUserById(id);
        userImage.setUser(user);
        userImage.setFileName(image.getOriginalFilename());
        userImage.setImage(BlobProxy.generateProxy(image.getBytes()));

        userImageService.addUserImage(userImage);
        return "Image successfully upload.";
    }
}
